const express = require('express');
const path = require('path');
const pug = require('pug');

const app = express();
const port = 3000;

app.use((req, res, next) => {
    console.log('Time:', Date.now());
    next();
})

app.get('/', (req, res) => {
    res.render('middleware');
})

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
});

app.set("view engine", "pug");
app.set('views', path.join(__dirname, 'views'));