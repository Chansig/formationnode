
const productsManager = require('../../workshop/src/productsManager');
const mongoose = require('mongoose');
const Product = require('../../workshop/models/product');

mongoose.connect('mongodb://localhost/myapp');
var db = mongoose.connection;
db
    .on('error', () => {
        console.error.bind(console, 'connection error:');
    })
    .on('connected', () => {

        productsManager.getAllProducts((err, products) => {
            if (!err) {
                Product.insertMany(products, function (err) {
                    if (!err) {
                        console.log(err);
                    }
                });
            } else {
                console.log(err)
            }
        })
    });