const userManager = require('../src/usersManager');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/myapp');
var db = mongoose.connection;
db
    .on('error', () => {
        console.error.bind(console, 'connection error:');
    })
    .on('connected', () => {
        userManager.cryptUser("admin", "admin");
        userManager.cryptUser("jojo", "jojo");
    });