
const mongoose = require('mongoose');
const User = require('../../workshop/models/user');

mongoose.connect('mongodb://localhost/myapp');
var db = mongoose.connection;
db
    .on('error', () => {
        console.error.bind(console, 'connection error:');
    })
    .on('connected', () => {
        users = [
            {
                "id": 1,
                "username": "admin",
                "password": "admin",
                "creation_date": "2018-12-19"
            },
            {
                "id": 2,
                "username": "jojo",
                "password": "jojo",
                "creation_date": "2018-12-19"
            }
        ];
        User.insertMany(users, function (err) {
            if (!err) {
                console.log(err);
            }
        });
    });