const mongoose = require('mongoose');
const order = {
    product: { type: 'ObjectId', ref: 'Product' },
    user: { type: 'ObjectId', ref: 'User' },
    "creation_date": Date,
    "price": Number,
    "currency": String
}
const Order = mongoose.model('Order', new mongoose.Schema(order));

module.exports = Order;