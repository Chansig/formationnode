const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const usermanager = require('../src/usersManager');
const user = {
    "id": Number,
    "username": String,
    "password": String,
    "creation_date": Date
}

const schema = new mongoose.Schema(user);

schema.methods.validPassword = async function (pwd) {
    try {
        return await bcrypt.compare(pwd, this.password);
    }
    catch (e) {
        console.log('errr', e);
    }
};

const User = mongoose.model('User', schema);

module.exports = User;