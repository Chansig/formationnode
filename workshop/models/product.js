const mongoose = require('mongoose');
const model = {
    "id": Number,
    "name": String,
    "auteur": String,
    "description": String,
    "USD_price": Number,
    "EUR_price": Number,
    "file_link": String,
    "creation_date": Date,
    "orders_counters": Number
}
const Product = mongoose.model('Product', new mongoose.Schema(model));

module.exports = Product;