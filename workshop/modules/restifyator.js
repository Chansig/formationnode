const Product = require('../models/product');
const Order = require('../models/order');
const methodOverride = require('method-override');
const restify = require('express-restify-mongoose');
const express = require("express");
const expressRouter = express.Router();

/**
 * 
 */
class Restifyator {
    /**
     *
     */
    constructor(app, router) {

        this.app = app || null;
        this.app.use(methodOverride());
        this.router = router || expressRouter;
        this.app.use(this.router);
        return this;
    }
    /**
     * Create restfull api for Product
     */
    restifyProduct() {
        restify.serve(this.router, Product);
    }
    /**
    * Create restfull api for Order
    */
    restifyOrder() {
        restify.serve(this.router, Order);
    }
};

/**
 * 
 * @param {*} app 
 */
function init(app) {
    return new Restifyator(app);
}

module.exports = init;
