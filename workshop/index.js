require('./modules/mongogo')();

const host = 'jojolasticot.com';
const port = 8000;
const express = require("express");
const bodyParser = require('body-parser');
const usermanager = require('./src/usersManager');

// Express
const app = express();
app.use(express.static('./public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.set('trust proxy', 1);

// Router
const router = express.Router();
app.use(router);

const productsManager = require('./src/productsManager');
const restifyator = require('./modules/restifyator')(app, router);
const ordersManager = require('./src/ordersManager');
const User = require('./models/user');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const helmet = require('helmet');

// Google connect
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

passport.use(new GoogleStrategy({
    clientID: '833226269060-1s0djod2gvcr5770m19ubdj6scb0uq2a.apps.googleusercontent.com',
    clientSecret: 'IHGJyWRU055_fKGWmdh0B8Qg',
    callbackURL: "http://jojolasticot.com:8000/auth/google/callback"
},
    async function (token, tokenSecret, profile, done) {
        console.log(profile);
        return await usermanager.findOrCreate({ profile });
    }
));

app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }));

app.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/login' }),
    function (req, res) {
        res.redirect('/');
    });

// Helmet
app.use(helmet());

debugger;

// Session
app.use(session({
    secret: 'jojo lasticot',
    resave: false,
    saveUninitialized: true,
    cookie: {}
}))

// Passport
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        User.findOne({ username: username }, async (err, user) => {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, { message: 'Incorrect username.' });
            }
            const valid = await user.validPassword(password);
            if (!true === valid) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            return done(null, user);
        });
    }
));

// Routes
/**
 * 
 */
app.get('/', function (req, res) {
    productsManager.getAllProducts((err, products) => {
        if (!err) {
            res.render('index.ejs', { products: products, user: req.user });
        } else {
            res.send('yolo');
        }
    })
})

// Orders
/**
 * 
 */
app.get(
    '/order/:id',
    function (req, res) {

        let user = req.user;
        if (!user) {
            res.send(JSON.stringify({ redirect: '/login' }));
        }

        var id = req.params.id;

        productsManager.orderProductById(id, (err, product) => {
            if (!err) {
                // Add order
                ordersManager.addOrder(product, user)
                    .then((order) => {
                        res.send(JSON.stringify({ product: product, order: order }));
                    })
                    .catch((e) => {
                        res.send(JSON.stringify({ error: 'error' }));
                    });

            } else {
                res.send(JSON.stringify(null));
            }
        })
    })


// Orders
/**
 * 
 */
app.get(
    '/orders',
    function (req, res) {

        let user = req.user;

        if (!user) {
            res.redirect('/login?' + encodeURIComponent('redirect/orders'));
        }

        // Add order
        ordersManager.getUserOrder(user)
            .then((orders) => {
                res.render('orders.ejs', { orders: orders, user: req.user ? req.user : null });
            })
            .catch((e) => {
                res.send(JSON.stringify({ error: 'error' }));
            });

    })
/**
 * 
 */
app.get(
    '/products',
    function (req, res) {
        let user = req.user;

        if (!user) {
            res.redirect('/login?redirect=' + encodeURIComponent('/products'));
        }

        // Add order
        ordersManager.getUserProducts(user)
            .then((products) => {
                res.render('products.ejs', { products: orders, user: req.user ? req.user : null });
            })
            .catch((e) => {
                res.send(JSON.stringify({ error: e }));
            });

    })

/**
 * 
 */
app.get('/login', function (req, res) {


    const redirect = req.query.redirect
    req.session.redirect = redirect;
    res.render('login.ejs', { user: req.user ? req.user : null });
})

/**
 * 
 */
app.post('/login',
    passport.authenticate('local'),
    function (req, res) {
        // If this function gets called, authentication was successful.
        // `req.user` contains the authenticated user.
        res.redirect('/');
    });

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

/**
 * 
 */
app.listen(port, host, function () {
    //console.log('App listening on port 80!')
}).on('error', () => {
    console.error.bind(console, 'connection error:');
})

restifyator.restifyProduct();
restifyator.restifyOrder();