var productManager = {
    load: function (id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let response = JSON.parse(xhttp.responseText);
                if (response.hasOwnProperty('redirect')) {
                    document.location = response.redirect;//+ '?redirect='+ encodeURIComponent(document.location.href);
                    return false;
                } else {
                    alert(xhttp.responseText);
                }
            }
        };
        xhttp.open("GET", "/order/" + id, true);
        xhttp.send();
    }
}

document.querySelectorAll('.btn').forEach(function (btn) {
    btn.addEventListener("click", function (event) {
        var id = event.target.id;
        var btn = id.substring(4);
        productManager.load(btn);
    });
});
