const Product = require('../models/product');

class ProductsManager {
    /**
     *
     */
    constructor() {

        return this;
    }


    /**
     * Get all products from json
     */
    getAllProducts(cb) {
        Product.find({}, (err, products) => {
            if (!err) {
                try {
                    cb(null, products);
                    return;
                } catch (e) {
                    cb(e);
                    return;
                }
            } else {
                cb(err);

                return;
            }
        });
    };

    /**
     * Get one product from products
     *
     * @param {*} id
     * @param {*} cb
     */

    orderProductById(id, cb) {
        Product.findOneAndUpdate({ id: id }, { $inc: { orders_counters: 1 } }, { new: true }, (err, modified) => {
            if (!err) {
                cb(null, modified);
            } else {
                cb(err);
            }
        });
    }
};



module.exports = new ProductsManager();
