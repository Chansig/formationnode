const User = require('../models/user');
const bcrypt = require('bcrypt');
const saltRounds = 10;

class usersManager {
    /**
     * 
     */
    constructor() {
    }

    /**
     * 
     * @param {*} username 
     */
    async  cryptUser(username, password) {
        try {
            const salt = await bcrypt.genSalt(saltRounds);
            const hash = await bcrypt.hash(password, salt);
            let user = await User.findOneAndUpdate({ username: username }, { password: hash }, { new: true });

            console.log('admin Password hashed', user);
        }
        catch (e) {
            console.log(e);
        }
    }

    async  getUserById(id) {
        return await User.find({ 'id': id })
    }

    async  createUser(user) {
        return await User.find({ 'username': username })
    }
    /**
     * 
     * @param {*} username 
     */
    async  findOrCreate(profile) {
        try {
            let user = await this.getUserById(profile.id);

            if (user) {
                return user;
            }

            const salt = await bcrypt.genSalt(saltRounds);
            const hash = await bcrypt.hash(profile.id, salt);
            let json = [
                {
                    "id": profile.id,
                    "username": profile.displayName,
                    "password": hash,
                    "creation_date": Date.now()
                }
            ];
            console.log(profile);

            return await User.insertMany(users);
        }
        catch (e) {
            console.log(e);
        }
    }
};

module.exports = new usersManager();