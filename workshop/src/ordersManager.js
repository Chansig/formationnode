const fs = require("fs");
const readline = require("readline")
const Product = require('../models/product');
const Order = require('../models/order');
const User = require('../models/user');

class ordersManager {

    /**
     *
     */
    constructor() {
    }

    /**
     * 
     * @param {*} product 
     * @param {*} user 
     */
    async addOrder(product, user) {
        var order = [
            {
                product: product._id,
                user: user._id,
                "creation_date": Date.now(),
                "price": product.price
            }
        ]
        try {
            return await Order.insertMany(order);
        } catch (e) {

        }
    }
    /**
     * 
     * @param {*} user 
     */
    async getUserOrder(user) {
        return await Order.find({ user: user._id })
            .populate('user')
            .populate('product')
            .then((orders) => {
                return orders;
            });
    }

    /**
     * // TODO populate
     * @param {*} user 
     */
    async getUserProducts(user) {
        try {
            let products = await Order.aggregate(
                [
                    { "$match": { user: user._id } },
                    {
                        "$lookup": {
                            "from": "Product",
                            "localField": "product",
                            "foreignField": "product",
                            "as": "groupedProduct"
                        }
                    },
                    {
                        "$group": {
                            _id: "$groupedProduct"
                        }
                    }
                ]
            )
            //.populate();
            console.log(products);

            return products;

        } catch (e) {
            console.log(e);
        }
    }
};



module.exports = new ordersManager();