
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/test2');
var db = mongoose.connection;
db
    .on('error', () => {
        console.error.bind(console, 'connection error:');
    })
    .on('connected', () => {
        console.log('connected')
    });

var model = {
    "id": Number,
    "name": String,
    "auteur": String,
    "description": String,
    "USD_price": Number,
    "EUR_price": Number,
    "file_link": String,
    "creation_date": Date,
    "orders_counters": Number
}

var schema = new mongoose.Schema(model);
var Product = mongoose.model('Poduct', schema);

console.log('Poduct schema Created!');

db.close();
