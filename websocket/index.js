const http = require('http');
var fs = require('fs');

let app = http.createServer(handler);

const io = require('socket.io')(app);

app.listen(8000);

io.on('connection', socket => {
    socket.emit('news', { hello: 'world (Ton voisin est plus sympa, il a dit Hello Chansig. Il a raison mais bon, faut pas lui dire' });
});


function handler(req, res) {
    fs.readFile(__dirname + '/index.html',
        (err, data) => {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }

            res.writeHead(200);
            res.end(data);
        });
}