const fs = require("fs");
const readline = require("readline")

class productsManager {

    /**
     * 
     */
    constructor() {
        console.log('init productsManager');

        this.productFile = `${__dirname}/products.json`;
    }

    /**
     * Get all products from json
     */
    getAllProducts(cb) {
        fs.readFile(this.productFile, 'utf8', (err, products) => {
            if (!err) {
                try {
                    var p = JSON.parse(products);
                    cb(null, p);

                    return;
                } catch (e) {
                    cb(e);

                    return;
                }
            } else {
                cb(err);

                return;
            }
        });

    };

    /**
     * Get one product from products
     * 
     * @param {*} id 
     * @param {*} cb 
     */

    orderProductById(id, cb) {
        this.getAllProducts((err, products) => {
            if (!err) {
                let found = products.find((product) => {
                    return id == product.id;
                });

                if (found) {
                    found['orders_counters']++;
                    fs.writeFile(this.productFile, JSON.stringify(products, null, 2), function () {
                        cb(null, found);
                    });
                } else {
                    cb("Produit introuvable");
                }
            } else {
                cb("Désolé, Une erreur est survenue");
            }
        });
    }


};

module.exports = new productsManager();