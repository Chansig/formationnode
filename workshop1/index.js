const fs = require("fs");
const readline = require("readline")
const productFile = `${__dirname}/products.json`;

(function init() {

    var rl = readline.createInterface(
        {
            input: process.stdin,
            output: process.stdout
        }
    );

    rl.on('line', function (id) {
        orderProductById(id);
    });

})();


/**
 * 
 */
function getAllProducts() {
    fs.readFile(productFile, 'utf8', (err, products) => {
        if (!err) {
            var p = JSON.parse(products);
            console.log('Voici les produits disponibles : ');
            p.forEach(displayProduct);
        } else {
            console.log("Désolé, Une erreur est survenue");
        }
    });
}

/**
 * 
 * @param {*} product 
 */
function displayProduct(product) {
    var line = `[${product.id}] - [${product.name}] / [${product.EUR_price}] / [${product.orders_counters}]`;
    console.log(line);
}

/**
 * 
 * @param {*} id 
 */
function orderProductById(id) {
    fs.readFile(productFile, 'utf8', (err, products) => {
        if (!err) {
            let p = JSON.parse(products);
            let found = p.find((product) => {
                return id == product.id;
            });

            if (found) {
                found['orders_counters']++;
                fs.writeFile(productFile, JSON.stringify(p, null, 2), function () {
                    console.log(`Commande terminée. Voici votre fichier : ${found.file_link}`);
                });
            } else {
                console.log("Produit introuvable");
            }
        } else {
            console.log("Désolé, Une erreur est survenue");
        }
    });
}

