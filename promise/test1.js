const myPromise = new Promise((resolve, reject) => {

    reject("bug");

    let t = setTimeout(() => {
        resolve('fine 1')
    }, 2000)

    clearTimeout(t);
    reject("bug 2");
})
    .then((response) => {
        console.log("log 1 : " + response);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve('fine 2')
            }, 2000)
        })
            .then((response) => {
                console.log("log 5 : " + response);

                return response;
            })
    })
    .then((response) => {
        console.log("log 2 : " + response);

        return response;
    })
    .then((response) => {
        console.log("log 3 : " + response);
    })
    .catch((err) => {
        console.log("log 4 : " + err);
    })