const myPromise = new Promise((resolve, reject) => {


    let t = setTimeout(() => {
        resolve(1)
    }, 1000)

})
    .then((result) => {
        console.log("log 1 : " + result);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(result * 2)
            }, 2000)
        })
    })
    .then((result) => {
        console.log("log 2 : " + result);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(result * 2)
            }, 2000)
        })
    })
    .then((result) => {
        console.log("log 3 : " + result);

        result * error;
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(result * 2)
            }, 2000)
        })
    })
    .then((result) => {
        console.log("log 4 : " + result);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(result * 2)
            }, 2000)
        })
    })
    .catch((err) => {
        console.log("error caught : ", err);
    })