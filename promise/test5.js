function resolveAfter2s() {
    return new Promise((resolve, reject) => {
        setTimeout((param) => {
            resolve('resolved');
        }, 2000);
    });
}

var asyncCall = async function () {
    console.log('calling');
    try {
        var result = await resolveAfter2s();
        console.log(result);
        result = await resolveAfter2s();
        return result;
    } catch (e) {
        console.log(e);
    }
}

asyncCall().then((value) => {
    console.log('fini : ' + value);
});