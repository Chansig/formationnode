var http = require('http');


const server = http.createServer((req, res) => {

    if (req.method === 'POST') {
        let body = '';

        req.on('data', chunk => {
            console.log(chunk)
            body += chunk.toString();
            console.log(body)
        });

        req.on('end', () => {
            console.log(body);
            res.end('ok')
        })
    }
}).listen(8080);

