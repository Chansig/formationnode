try{
    const a = 1;
    a = 2;
    console.log("ok")
} catch(e){
    console.log('ko');
}

try{
    const a = {b: 1};
    a.b = 2;
    console.log("ok");
    console.log(a);
} catch(e){
    console.log(e);
}